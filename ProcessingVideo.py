# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 23:07:21 2019

@author: Marco Chiappini
"""

import cv2 as cv2
import os as os
import numpy as np
#Cn <--GreyScaleFrame, now without significant value
Cn = np.zeros([1080,1920],dtype=np.uint8) # size of image, only for initialization
#Cn previous used in the next elaboration while i'll read the video, now without significant value
CnPrevious = np.zeros([1080,1920],dtype=np.uint8) # size of image, only for initialization
fgbg = cv2.createBackgroundSubtractorMOG2()
cap = cv2.VideoCapture("./office1video1.mp4") #name file to make parametric
#create Path arbitrary
root = "D:"
child1 = "frames"
childSelectorJn = "Jn\\"
childSelectorSn = "Sn\\"
 # os.path.join <--- function that construct path for the current operating systems
pathJn = os.path.join(root,child1,childSelectorJn) #with the names of folder passed with the argument
pathSn = os.path.join(root,child1,childSelectorSn)
#i'll write every different collection of frames in different folders grouped by type.
#Jn <--- Foreground
#Sn <--- Spatial motion information

while not cap.isOpened():
    cap = cv2.VideoCapture("./office1video1.mp4")
    cv2.waitKey(1000)
    print ("Wait for the header")

pos_frame = cap.get(cv2.CAP_PROP_POS_FRAMES) # indicates the current frame selected
while True:
    flag, frame = cap.read()
    if flag:
        # The frame is ready and already captured
        #cv2.imshow('video', frame) it's commented because i don't have to show it
        if(pos_frame >= 1):
            CnPrevious = Cn # here i assign the previous Cn frame 
        Cn = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)#to gray scale image
                                                    #Cn <--grey scale frame

        Bn = fgbg.apply(Cn) #Applying DBM Gaussian mixture-Based Dynamic BackGround Modelling
                            #Bn <-- Grey Scale Background Frame
        Jn = abs(Cn - Bn)   #Jn <--- Difference Between Grey Scale Frame and Grey Scale BackGround Frame
                            #Jn <--- Foreground Object in a Frame  
        cv2.imwrite(pathJn +"Jn-" + str(pos_frame) + ".jpg", Jn) #Write to the desidered path Jn on fs
        
        Sn = abs(Cn - CnPrevious)   #Calculating Consecutive Frame Difference Between
                                    #the last grey scale frame read and the previous
        cv2.imwrite(pathSn +"Sn-" + str(pos_frame-1) + ".jpg", Sn) #write to the desidered path Sn on fs
                                    #Sn <-- conntains the spatial motion information
        pos_frame = cap.get(cv2.CAP_PROP_POS_FRAMES)
        print (str(pos_frame)+" frames" + "value of i : " + str(i))
    else:
        # The next frame is not ready, so we try to read it again
        cap.set(cv2.CAP_PROP_POS_FRAMES, pos_frame-1)
        print ("frame is not ready")
        # It is better to wait for a while for the next frame to be ready
        cv2.waitKey(1000)

    if cv2.waitKey(10) == 27:
        break
    if cap.get(cv2.CAP_PROP_POS_FRAMES) == cap.get(cv2.CAP_PROP_FRAME_COUNT):
        # If the number of captured frames is equal to the total number of frames,
        # we stop
        break

